package tfile

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"fmt"
	"os"

	"github.com/jackpal/bencode-go"
	"github.com/ArthurLokhov/go-torrent/p2p"
)

const Port uint16 = 3000 // С помощью какого порта будет скачиваться .torrent файл.

// Описываем поля .torrent файла
type TorrentFile struct {
	Announce string
	Info     [20]byte
	Pieces   [][20]byte
	PLength  int
	Length   int
	Name     string
}

// Структуры, которые описывают уже полученную из .torrent файла информацию
type bencodeInfo struct {
	Pieces  string `bencode:"pieces"`
	PLength int    `bencode:"piece length"`
	Length  int    `bencode:"length"`
	Name    string `bencode:"name"`
}

type bencodeTorrent struct {
	Announce string      `bencode:"announce"`
	Info     bencodeInfo `bencode:"info"`
}

// Скачивание .torrent файла и сохранение полученного файла
func (t *TorrentFile) DownloadToFile(path string) error {
	var pid [20]byte
	_, err := rand.Read(pid[:])
	if err != nil {
		return err
	}

	peers, err := t.requestPeers(pid, Port)
	if err != nil {
		return err
	}

	torrent := p2p.Torrent{
		Peers:       peers,
		PID:         pid,
		InfoHash:    t.Info,
		PieceHashes: t.Pieces,
		PieceLength: t.PLength,
		Length:      t.Length,
		Name:        t.Name,
	}
	// Начинаем процесс скачивание
	buffer, err := torrent.Download()
	if err != nil {
		return err
	}

	// Создаем пустой файл, куда будет сохранена готовая информация
	outFile, err := os.Create(path)
	if err != nil {
		return err
	}
	defer outFile.Close()
	_, err = outFile.Write(buffer)
	if err != nil {
		return err
	}
	return nil
}

// Парсим .torrent файл
func Open(path string) (TorrentFile, error) {
	file, err := os.Open(path)
	if err != nil {
		return TorrentFile{}, err
	}
	defer file.Close()

	bcode := bencodeTorrent{}
	err = bencode.Unmarshal(file, &bcode)
	if err != nil {
		return TorrentFile{}, err
	}
	return bcode.toTorrentFile()
}

// Создаем Hash .torrent файла для сравнения с трекером
func (binfo *bencodeInfo) hash() ([20]byte, error) {
	var buffer bytes.Buffer
	err := bencode.Marshal(&buffer, *binfo)
	if err != nil {
		return [20]byte{}, err
	}
	hash := sha1.Sum(buffer.Bytes())
	return hash, nil
}

// Форматируем Hash файла
func (i *bencodeInfo) splitPieceHashes() ([][20]byte, error) {
	hashLength := 20 // Length of SHA-1 hash
	buffer := []byte(i.Pieces)
	if len(buffer)%hashLength != 0 {
		err := fmt.Errorf("Received malformed pieces of length %d", len(buffer))
		return nil, err
	}
	numHashes := len(buffer) / hashLength
	hashes := make([][20]byte, numHashes)

	for i := 0; i < numHashes; i++ {
		copy(hashes[i][:], buffer[i*hashLength:(i+1)*hashLength])
	}
	return hashes, nil
}

// Превращаем bencodeInfo и bencodeTorrent в TorrentFile структуру
func (bencode *bencodeTorrent) toTorrentFile() (TorrentFile, error) {
	info, err := bencode.Info.hash()
	if err != nil {
		return TorrentFile{}, err
	}
	pieces, err := bencode.Info.splitPieceHashes()
	if err != nil {
		return TorrentFile{}, err
	}
	t := TorrentFile{
		Announce: bencode.Announce,
		Info:     info,
		Pieces:   pieces,
		PLength:  bencode.Info.PLength,
		Length:   bencode.Info.Length,
		Name:     bencode.Info.Name,
	}
	return t, nil
}
