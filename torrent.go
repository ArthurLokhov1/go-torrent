package main

import (
	"log"
	"os"

	"github.com/ArthurLokhov/go-torrent/tfile"
)

func main() {
	inputPath := os.Args[1]  // Где находится .torrent
	outputPath := os.Args[2] // Куда сохранить готовый файл

	tf, err := tfile.Open(inputPath)
	if err != nil {
		log.Fatal(err)
	}

	err = tf.DownloadToFile(outputPath)
	if err != nil {
		log.Fatal(err)
	}
}
