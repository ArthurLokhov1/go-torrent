package bitsfield

type BitsField []byte

// HasPiece указывает имеет ли битовое поле определенный набор индексов
func (bf BitsField) HasPiece(index int) bool {
	byteIndex := index / 8
	offset := index % 8
	if byteIndex < 0 || byteIndex >= len(bf) {
		return false
	}
	return bf[byteIndex]>>uint(7-offset)&1 != 0
}

// SetPiece устанавливает бит в битовом поле
func (bf BitsField) SetPiece(index int) {
	byteIndex := index / 8
	offset := index % 8

	if byteIndex < 0 || byteIndex >= len(bf) {
		return
	}
	bf[byteIndex] |= 1 << uint(7 - offset)
}
